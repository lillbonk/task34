import React from "react";

const RegisterForm = props => {

  

  const onRegisterClicked = e => {
    console.log('Event: ', e.target);
    props.click();
  };

  return (
    <form>
      <div>
        <label>Username: </label>
        <input type="text" name="username" placeholder="Enter a username" />
      </div>

      <div>
        <label>Password: </label>
        <input type="password" name="pasword" placeholder="Enter a password" />
      </div>

      <button type="button" onClick={ onRegisterClicked }>Register</button>
    </form>
  );
};

export default RegisterForm;
