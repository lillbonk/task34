import React from "react";

const LoginForm = () => {
  return (
    <form>
      <div>
        <label>Username: </label>
        <input type="text" name="username" placeholder="Enter your username" />
      </div>

      <div>
        <label>Password: </label>
        <input type="password" name="password" placeholder="Enter your password"/>
      </div>

        <button>Login</button>
    </form>
  );
};

export default LoginForm;
